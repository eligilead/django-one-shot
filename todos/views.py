from django.shortcuts import render
from django.urls import reverse_lazy

from django.views.generic import ListView, DetailView, CreateView, UpdateView
from todos.models import TodoList, TodoItem

# Create your views here.
# context_object_name = you can name your class whatever you want
# .objects only used in views


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todo_list_list"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todo_listdetail"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    context_object_name = "todo_list_create"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])
        # need for most redirections


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])
